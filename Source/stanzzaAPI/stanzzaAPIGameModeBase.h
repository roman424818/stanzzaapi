// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Http.h"
#include "stanzzaAPIGameModeBase.generated.h"

USTRUCT(BlueprintType)
struct FUserProfil
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FString login;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FString password;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FString fingerprint;
};



UCLASS()
class STANZZAAPI_API AstanzzaAPIGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
		AstanzzaAPIGameModeBase();
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
		void SendHTTPPost();

	UFUNCTION(BlueprintCallable)
		void SendHTTPGet();

protected:

	FHttpModule* Http;
	void OnProcessRequestComplete(FHttpRequestPtr Request, FHttpResponsePtr Response, bool Success);

public:
	FTimerHandle TimerGetParams;
};
