// Copyright Epic Games, Inc. All Rights Reserved.


#include "stanzzaAPIGameModeBase.h"
#include "JsonObjectConverter.h"
#include "UObject/ConstructorHelpers.h"

AstanzzaAPIGameModeBase::AstanzzaAPIGameModeBase()
{
	Http = &FHttpModule::Get();
}

void AstanzzaAPIGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	SendHTTPPost();
}

void AstanzzaAPIGameModeBase::SendHTTPPost()
{
	TSharedRef<IHttpRequest, ESPMode::ThreadSafe> Request = Http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &AstanzzaAPIGameModeBase::OnProcessRequestComplete);
	Request->SetURL("https://stanzza-api.aicrobotics.ru/api/auth/login");
	Request->SetVerb("POST");
	Request->SetHeader(TEXT("Content-Type"), TEXT("application/json"));
	FString JsonString;
	FUserProfil UserProfil;
	UserProfil.login = TEXT("candidate@asdfizac.org");
	UserProfil.password = TEXT("asdfizac_tpsxP3f5*s");
	UserProfil.fingerprint = TEXT("5ddd5");
	FJsonObjectConverter::UStructToJsonObjectString(UserProfil, JsonString);
	Request->SetContentAsString(JsonString);
	Request->ProcessRequest();
}

void AstanzzaAPIGameModeBase::SendHTTPGet()
{
	TSharedRef<IHttpRequest, ESPMode::ThreadSafe> Request = Http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &AstanzzaAPIGameModeBase::OnProcessRequestComplete);
	Request->SetURL("https://stanzza-api.aicrobotics.ru/api/v1/catalog/06-02-013");
	Request->SetVerb("GET");
	Request->SetHeader(TEXT("Content-Type"), TEXT("application/json"));
	Request->ProcessRequest();
}

void AstanzzaAPIGameModeBase::OnProcessRequestComplete(FHttpRequestPtr Request, FHttpResponsePtr Response, bool Success)
{
	if (Success)
	{
		UE_LOG(LogTemp, Warning, TEXT("%s"), *Response->GetContentAsString());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("FAILED"));
	}
}
