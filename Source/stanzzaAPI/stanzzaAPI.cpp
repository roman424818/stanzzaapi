// Copyright Epic Games, Inc. All Rights Reserved.

#include "stanzzaAPI.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, stanzzaAPI, "stanzzaAPI" );
